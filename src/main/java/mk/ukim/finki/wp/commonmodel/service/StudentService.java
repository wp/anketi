package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.Student;

import java.util.List;

public interface StudentService {
    Student findByIndex(String index);
    List<Student> findAll();
    Student save(Student s);
}
