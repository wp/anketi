package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.CourseGroup;
import mk.ukim.finki.wp.commonmodel.repository.CourseGroupRepository;
import mk.ukim.finki.wp.commonmodel.service.CourseGroupService;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseGroupServiceImpl implements CourseGroupService {

    private final CourseGroupRepository courseGroupRepository;

    public CourseGroupServiceImpl(CourseGroupRepository courseGroupRepository) {
        this.courseGroupRepository = courseGroupRepository;
    }

    @Override
    public List<CourseGroup> findAll() {
        return this.courseGroupRepository.findAll();
    }

}
