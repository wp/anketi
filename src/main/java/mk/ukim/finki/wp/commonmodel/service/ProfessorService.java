package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.Professor;

import java.util.List;

public interface ProfessorService {

    List<Professor> findAllByIds(List<String> professorIds);

    List<Professor> findAll();

    Professor findById(String professorId);

    List<Professor> findAllSorted();
}
