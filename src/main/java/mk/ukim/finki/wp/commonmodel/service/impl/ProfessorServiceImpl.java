package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.Professor;
import mk.ukim.finki.wp.commonmodel.repository.ProfessorRepository;
import mk.ukim.finki.wp.commonmodel.service.ProfessorService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;

    public ProfessorServiceImpl(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    @Override
    public List<Professor> findAllByIds(List<String> professorIds) {
        return this.professorRepository.findAllById(professorIds);
    }

    @Override
    public List<Professor> findAll() {
        return this.professorRepository.findAll();
    }

    @Override
    public Professor findById(String professorId) {
        return this.professorRepository.findById(professorId).orElse(null);
    }

    @Override
    public List<Professor> findAllSorted() {
        return this.professorRepository.findAll(Sort.by(Sort.Direction.DESC, "name"));
    }
}
