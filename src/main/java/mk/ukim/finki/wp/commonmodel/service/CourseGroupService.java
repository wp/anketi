package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.CourseGroup;

import java.util.List;

public interface CourseGroupService {

    List<CourseGroup> findAll();

}
