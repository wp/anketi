package mk.ukim.finki.wp.commonmodel.repository;

import mk.ukim.finki.wp.commonmodel.base.Course;
import mk.ukim.finki.wp.commonmodel.base.Professor;
import mk.ukim.finki.wp.commonmodel.evaluation.CourseProfessorEvaluation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseProfessorEvaluationRepository extends JpaRepository<CourseProfessorEvaluation, Long> {
    Page<CourseProfessorEvaluation> findCourseProfessorEvaluationsByCourseIn (List<Course> courses, Pageable pageable);
    Page<CourseProfessorEvaluation> findAllByProfessor (Professor professor, Pageable pageable);
    Page<CourseProfessorEvaluation> findCourseProfessorEvaluationsByCourseInAndProfessor (List<Course> courses, Professor professor, Pageable pageable);

}