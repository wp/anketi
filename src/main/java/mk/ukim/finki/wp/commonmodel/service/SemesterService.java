package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.Semester;

import java.util.List;

public interface SemesterService {

    List<Semester> findAllDescending();

    Semester findById(String semesterCode);
}
