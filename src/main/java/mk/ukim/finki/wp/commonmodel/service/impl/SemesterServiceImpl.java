package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.Semester;
import mk.ukim.finki.wp.commonmodel.repository.SemesterRepository;
import mk.ukim.finki.wp.commonmodel.service.SemesterService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SemesterServiceImpl implements SemesterService {
    private final SemesterRepository semesterRepository;

    public SemesterServiceImpl(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }


    @Override
    public List<Semester> findAllDescending() {
        return this.semesterRepository.findAll(Sort.by(Sort.Direction.DESC, "startDate"));
    }

    @Override
    public Semester findById(String semesterCode) {
        return this.semesterRepository.findById(semesterCode).orElse(null);
    }
}
