package mk.ukim.finki.wp.commonmodel.repository;

import mk.ukim.finki.wp.commonmodel.base.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, String> {

}
