package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.repository.StudentRepository;
import mk.ukim.finki.wp.commonmodel.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student findByIndex(String index) {
        return this.studentRepository.findById(index).orElse(null);
    }

    @Override
    public List<Student> findAll() {
        return this.studentRepository.findAll();
    }

    @Override
    public Student save(Student s) {
        return this.studentRepository.save(s);
    }

}
