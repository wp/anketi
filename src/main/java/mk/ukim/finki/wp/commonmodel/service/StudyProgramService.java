package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.StudyProgram;

import java.util.List;

public interface StudyProgramService {

    List<StudyProgram> findAll();
}
