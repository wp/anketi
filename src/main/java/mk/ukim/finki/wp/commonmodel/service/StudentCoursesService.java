package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.CourseGroup;
import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.base.StudentCourses;

import java.util.List;

public interface StudentCoursesService {
    List<StudentCourses> findAll();

    List<StudentCourses> findAllByStudent(Student student);

    StudentCourses save(Student student, CourseGroup courseGroup);

}
