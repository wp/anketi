package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.*;
import mk.ukim.finki.wp.commonmodel.evaluation.CourseProfessorEvaluation;
import mk.ukim.finki.wp.commonmodel.repository.CourseProfessorEvaluationRepository;
import mk.ukim.finki.wp.commonmodel.service.CourseProfessorEvaluationService;
import mk.ukim.finki.wp.commonmodel.service.CourseService;
import mk.ukim.finki.wp.commonmodel.service.ProfessorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseProfessorEvaluationServiceImpl implements CourseProfessorEvaluationService {

    private final CourseProfessorEvaluationRepository courseProfessorEvaluationRepository;
    private final CourseService courseService;
    private final ProfessorService professorService;

    public CourseProfessorEvaluationServiceImpl(CourseProfessorEvaluationRepository courseProfessorEvaluationRepository,
                                                CourseService courseService,
                                                ProfessorService professorService) {
        this.courseProfessorEvaluationRepository = courseProfessorEvaluationRepository;
        this.courseService = courseService;
        this.professorService = professorService;
    }

    @Override
    public List<CourseProfessorEvaluation> findAll() {
        return this.courseProfessorEvaluationRepository.findAll();
    }

    @Override
    public CourseProfessorEvaluation save(Course course, Professor professor, short grade, String comment) {
        CourseProfessorEvaluation courseProfessorEvaluation = new CourseProfessorEvaluation();
        courseProfessorEvaluation = this.courseProfessorEvaluationRepository.save(new CourseProfessorEvaluation());

        courseProfessorEvaluation.setProfessor(professor);
        courseProfessorEvaluation.setCourse(course);
        courseProfessorEvaluation.setGrade(grade);
        courseProfessorEvaluation.setComment(comment);


        return this.courseProfessorEvaluationRepository.save(courseProfessorEvaluation);
    }

    @Override
    public Page<CourseProfessorEvaluation> findAllByPage(Integer pageNumber, Integer pageNumberOfItems) {
        return this.courseProfessorEvaluationRepository.
                findAll(PageRequest.of(pageNumber-1, pageNumberOfItems, Sort.by(Sort.Direction.DESC, "course")));
    }

    @Override
    public Page<CourseProfessorEvaluation> filter(String subjectId, String professorId, Integer pageNumber, Integer pageNumberOfItems) {
        
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageNumberOfItems);

        if (!subjectId.isEmpty() && professorId.isEmpty()) {
//            filter by course
            List<Course> courses = this.courseService.findAllCoursesContainingSubject(subjectId);
            return this.courseProfessorEvaluationRepository.findCourseProfessorEvaluationsByCourseIn(courses, pageRequest);
        }
        if (subjectId.isEmpty() && !professorId.isEmpty()) {
//            filter by professor
            Professor professor = this.professorService.findById(professorId);
            return this.courseProfessorEvaluationRepository.findAllByProfessor(professor, pageRequest);
        }
        if (!subjectId.isEmpty()) {
//           filter by subject and professor
            List<Course> courses = this.courseService.findAllCoursesContainingSubject(subjectId);
            Professor professor = this.professorService.findById(professorId);
            return this.courseProfessorEvaluationRepository.findCourseProfessorEvaluationsByCourseInAndProfessor(courses, professor, pageRequest);

        }
//        else return all evaluations with specific order and pagination

        return this.courseProfessorEvaluationRepository.findAll(pageRequest);
    }
}
