package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.evaluation.CompletedStudentSemesterEvaluation;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CompletedStudentSemesterEvaluationService {
    List<CompletedStudentSemesterEvaluation> findAll();

    Page<CompletedStudentSemesterEvaluation> findCompletedSemesterEvaluationBySemesterAndStudent(String semesterId, String studentIndex, Integer pageNumber, Integer pageSize);

    CompletedStudentSemesterEvaluation save(String semesterCode, String studentIndex);

    Page<CompletedStudentSemesterEvaluation> findAllByPage(Integer pageNumber, Integer pageSize);

    Page<CompletedStudentSemesterEvaluation> filter(String semesterCode, String studentIndex, Integer pageNumber, Integer pageSize);

}
