package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.Subject;

public interface SubjectService {

    Subject findById(String id);
}
