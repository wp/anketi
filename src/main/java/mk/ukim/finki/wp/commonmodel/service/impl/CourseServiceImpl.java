package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.Course;
import mk.ukim.finki.wp.commonmodel.base.Subject;
import mk.ukim.finki.wp.commonmodel.repository.CourseRepository;
import mk.ukim.finki.wp.commonmodel.service.CourseService;
import mk.ukim.finki.wp.commonmodel.service.SubjectService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final SubjectService subjectService;

    public CourseServiceImpl(CourseRepository courseRepository, SubjectService subjectService) {
        this.courseRepository = courseRepository;
        this.subjectService = subjectService;
    }


    @Override
    public Course save(Course course) {
        return this.courseRepository.save(course);
    }

    @Override
    public List<Course> findAll() {
        return this.courseRepository.findAll();
    }

    @Override
    public List<Course> findAllByIds(List<String> courseIds) {
        return this.courseRepository.findAllById(courseIds);
    }

    @Override
    public Course findById(String courseId) {
        return this.courseRepository.findById(courseId).orElse(null);
    }

    @Override
    public List<Course> findAllCoursesContainingSubject(String subjectId) {
        Subject subject = this.subjectService.findById(subjectId);
        return this.courseRepository.findCoursesBySubject(subject);
    }
}
