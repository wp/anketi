package mk.ukim.finki.wp.commonmodel.service;

import mk.ukim.finki.wp.commonmodel.base.Course;
import mk.ukim.finki.wp.commonmodel.base.Professor;
import mk.ukim.finki.wp.commonmodel.evaluation.CourseProfessorEvaluation;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseProfessorEvaluationService {
    List<CourseProfessorEvaluation> findAll();

    CourseProfessorEvaluation save(Course course, Professor professor, short grade, String comment);

    Page<CourseProfessorEvaluation> findAllByPage(Integer pageNum, Integer pageNumberOfItems);

    Page<CourseProfessorEvaluation> filter(String courseCode, String professorId, Integer pageNum, Integer pageNumberOfItems);
}
