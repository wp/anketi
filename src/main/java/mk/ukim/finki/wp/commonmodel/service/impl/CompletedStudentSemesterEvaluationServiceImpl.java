package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.Semester;
import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.evaluation.CompletedStudentSemesterEvaluation;
import mk.ukim.finki.wp.commonmodel.repository.CompletedStudentSemesterEvaluationRepository;
import mk.ukim.finki.wp.commonmodel.service.CompletedStudentSemesterEvaluationService;
import mk.ukim.finki.wp.commonmodel.service.SemesterService;
import mk.ukim.finki.wp.commonmodel.service.StudentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompletedStudentSemesterEvaluationServiceImpl implements CompletedStudentSemesterEvaluationService {

    private final CompletedStudentSemesterEvaluationRepository completedStudentSemesterEvaluationRepository;
    private final StudentService studentService;
    private final SemesterService semesterService;

    public CompletedStudentSemesterEvaluationServiceImpl(
            CompletedStudentSemesterEvaluationRepository completedStudentSemesterEvaluationRepository,
            StudentService studentService, SemesterService semesterService) {

        this.completedStudentSemesterEvaluationRepository = completedStudentSemesterEvaluationRepository;
        this.studentService = studentService;
        this.semesterService = semesterService;
    }

    @Override
    public List<CompletedStudentSemesterEvaluation> findAll() {
        return this.completedStudentSemesterEvaluationRepository.findAll(Sort.by(Sort.Direction.DESC, "semester"));
//        TODO remove it when you can
    }

    @Override
    public Page<CompletedStudentSemesterEvaluation> findAllByPage(Integer pageNumber, Integer pageSize) {

      return this.completedStudentSemesterEvaluationRepository.
                        findAll(PageRequest.of(pageNumber-1, pageSize));
    }

    @Override
    public Page<CompletedStudentSemesterEvaluation> filter(String semesterCode, String studentIndex,
                                                           Integer pageNumber, Integer pageSize) {

        PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize);

        if (!semesterCode.isEmpty() && studentIndex.isEmpty()) {
//            filter by semester code
            Semester semester = this.semesterService.findById(semesterCode);
            return this.completedStudentSemesterEvaluationRepository.findAllBySemester(semester, pageRequest);
        }
        if (semesterCode.isEmpty() && !studentIndex.isEmpty()) {
//            Filter by student index
            Student student = this.studentService.findByIndex(studentIndex);
            return this.completedStudentSemesterEvaluationRepository.findAllByStudent(student, pageRequest);
        }
        if (!semesterCode.isEmpty()) {
            Semester semester = this.semesterService.findById(semesterCode);
            Student student = this.studentService.findByIndex(studentIndex);
            return this.completedStudentSemesterEvaluationRepository.findCompletedStudentSemesterEvaluationBySemesterAndStudent(semester, student, pageRequest);

//            filter by semester code and student index
        }
//        else return all evaluations with specific order and pagination

        return this.completedStudentSemesterEvaluationRepository.findByOrderBySemesterDesc(pageRequest);
    }


    @Override
    public Page<CompletedStudentSemesterEvaluation> findCompletedSemesterEvaluationBySemesterAndStudent(String semesterId, String studentIndex,
                                                                                                        Integer pageNumber, Integer pageSize) {
        Semester semester = this.semesterService.findById(semesterId);
        Student student = this.studentService.findByIndex(studentIndex);
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize);
        return this.completedStudentSemesterEvaluationRepository.findCompletedStudentSemesterEvaluationBySemesterAndStudent(semester, student, pageRequest);

    }

    @Override
    public CompletedStudentSemesterEvaluation save(String semesterCode, String studentIndex) {
        Semester semester = this.semesterService.findById(semesterCode);
        Student student = this.studentService.findByIndex(studentIndex);
        CompletedStudentSemesterEvaluation completedStudentSemesterEvaluation =
                this.completedStudentSemesterEvaluationRepository.save(new CompletedStudentSemesterEvaluation());

        completedStudentSemesterEvaluation.setSemester(semester);
        completedStudentSemesterEvaluation.setStudent(student);

        return this.completedStudentSemesterEvaluationRepository.save(completedStudentSemesterEvaluation);
    }
}
