package mk.ukim.finki.wp.commonmodel.web;

import mk.ukim.finki.wp.commonmodel.evaluation.CompletedStudentSemesterEvaluation;
import mk.ukim.finki.wp.commonmodel.service.SemesterService;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import mk.ukim.finki.wp.commonmodel.service.CompletedStudentSemesterEvaluationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Controller
@RequestMapping("/survey/completed")
public class CompletedStudentSemesterEvaluationController {

    private final CompletedStudentSemesterEvaluationService completedStudentSemesterEvaluationService;
    private final SemesterService semesterService;

    public CompletedStudentSemesterEvaluationController(CompletedStudentSemesterEvaluationService completedStudentSemesterEvaluationService,
                                                        SemesterService semesterService
    ) {
        this.completedStudentSemesterEvaluationService = completedStudentSemesterEvaluationService;
        this.semesterService = semesterService;
    }

    @GetMapping
    public String getAllSurveys(
            @RequestParam(required = false) String semesterCode,
            @RequestParam(required = false) String studentIndex,
            @RequestParam(required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(required = false, defaultValue = "2") Integer pageNumberOfItems,
            Model model
    ) {
//        TODO change page size before merge request
        Page<CompletedStudentSemesterEvaluation> page = null;

        if ((semesterCode == null && studentIndex == null) ||
                (Objects.equals(semesterCode, "null") && Objects.equals(studentIndex, "null"))) {
            page = this.completedStudentSemesterEvaluationService.findAllByPage(pageNum, pageNumberOfItems);
        } else {
            page = this.completedStudentSemesterEvaluationService.filter(semesterCode, studentIndex, pageNum, pageNumberOfItems);
        }

        model.addAttribute("semesters", this.semesterService.findAllDescending());
        model.addAttribute("semesterCode", semesterCode);
        model.addAttribute("surveys", page.getContent());
        model.addAttribute("currentPage", page.getNumber() + 1);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("filterIndex", studentIndex);
        return "completedSurveys";
    }
}
