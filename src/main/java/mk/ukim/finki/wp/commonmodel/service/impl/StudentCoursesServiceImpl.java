package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.CourseGroup;
import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.base.StudentCourses;
import mk.ukim.finki.wp.commonmodel.repository.StudentCoursesRepository;
import mk.ukim.finki.wp.commonmodel.service.StudentCoursesService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentCoursesServiceImpl implements StudentCoursesService {

    private final StudentCoursesRepository studentCoursesRepository;

    public StudentCoursesServiceImpl(StudentCoursesRepository studentCoursesRepository) {
        this.studentCoursesRepository = studentCoursesRepository;
    }

    @Override
    public List<StudentCourses> findAll() {
        return this.studentCoursesRepository.findAll();
    }

    @Override
    public List<StudentCourses> findAllByStudent(Student student) {
        return this.studentCoursesRepository.findAllByStudent(student);
    }


    @Override
    public StudentCourses save(Student student, CourseGroup courseGroup) {
        StudentCourses studentCourses = this.studentCoursesRepository.save(new StudentCourses());
        studentCourses.setStudent(student);
        studentCourses.setCourseGroup(courseGroup);
        return this.studentCoursesRepository.save(studentCourses);
    }
}
