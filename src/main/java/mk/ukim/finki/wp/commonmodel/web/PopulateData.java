package mk.ukim.finki.wp.commonmodel.web;

import mk.ukim.finki.wp.commonmodel.base.*;
import mk.ukim.finki.wp.commonmodel.repository.*;
import mk.ukim.finki.wp.commonmodel.service.CourseService;
import mk.ukim.finki.wp.commonmodel.service.StudentService;
import mk.ukim.finki.wp.commonmodel.service.StudyProgramService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/populate-data")
public class PopulateData {
    private final StudentService studentService;
    private final StudyProgramService studyProgramService;
    private final CourseService courseService;
    private final SemesterRepository semesterRepository;
    private final SubjectRepository subjectRepository;
    private final ProfessorRepository professorRepository;

    private final CourseGroupRepository courseGroupRepository;

    private final StudentCoursesRepository studentCoursesRepository;

    public PopulateData(StudentService studentService,
                        StudyProgramService studyProgramService,
                        CourseService courseService, SemesterRepository semesterRepository, SubjectRepository subjectRepository, ProfessorRepository professorRepository, CourseGroupRepository courseGroupRepository, StudentCoursesRepository studentCoursesRepository) {
        this.studentService = studentService;
        this.studyProgramService = studyProgramService;
        this.courseService = courseService;
        this.semesterRepository = semesterRepository;
        this.subjectRepository = subjectRepository;
        this.professorRepository = professorRepository;
        this.courseGroupRepository = courseGroupRepository;
        this.studentCoursesRepository = studentCoursesRepository;
    }


    @GetMapping
    public String populateData() {
//        List<Student> students = studentService.findAll();
//        if (students.isEmpty()) {
//            this.populateStudents();
//        }
        this.populateStudents();
        return "index.html";
    }

    private void populateStudents() {
        List<StudyProgram> studyPrograms = this.studyProgramService.findAll();
        List<String> names;
        Collections.addAll(names = new ArrayList<>(),
                "Marko", "Darko", "Sanja", "Goran", "Ivan", "Marija", "Sara", "Jana", "Marijana", "Mihail",
                "Dragana", "Anja", "Stojan", "Toni", "Stojna", "Daniela", "Joco", "Jordan", "Gabriela", "Sanela", "Borce");
        Random rand = new Random();


//        creatin 20 students dummy data
        for (int i = 0; i < 20; i++) {
            int index = 171100 + i;
            String name = names.get(rand.nextInt(names.size()));
            String email = String.format("%s%d@students.finki.ukim.mk", name, index);
            this.studentService.save(new Student(Integer.toString(index),
                    email,
                    name,
                    name,
                    names.get(rand.nextInt(names.size())),
                    studyPrograms.get(rand.nextInt(studyPrograms.size()))
            ));
        }

    // Creating 10 courses with professor and semester
        List<Semester> semestersWinter = this.semesterRepository.findAllBySemesterType(SemesterType.WINTER);
        List<Semester> semestersSummer = this.semesterRepository.findAllBySemesterType(SemesterType.SUMMER);
        List<Subject> winterSubjects = this.subjectRepository.findAllBySemester(SemesterType.WINTER);
        List<Subject> summerSubjects = this.subjectRepository.findAllBySemester(SemesterType.SUMMER);
        List<Professor> professors = this.professorRepository.findAll();

//        Course and course group dummy data
        for (int i = 0; i < 10; i++) {

            Subject subject = new Subject();
            Semester semester = new Semester();

            if (i < 5){
                subject = winterSubjects.get(rand.nextInt(winterSubjects.size()));
                semester = semestersWinter.get(rand.nextInt(semestersWinter.size()));
            } else {
                subject = summerSubjects.get(rand.nextInt(summerSubjects.size()));
                semester = semestersSummer.get(rand.nextInt(semestersSummer.size()));
            }

            Course course = new Course(Integer.toString(i),
                    semester,
                    subject,
                    subject,
                    subject,
                    20L,
                    1L);
            course = this.courseService.save(course);

            CourseGroup courseGroup = this.courseGroupRepository.save(new CourseGroup());
            courseGroup.setCourse(course);
            courseGroup.setProfessor(professors.get(rand.nextInt(professors.size())));
            this.courseGroupRepository.save(courseGroup);
        }

//        enroll students to course groups
        List<CourseGroup> courseGroups = this.courseGroupRepository.findAll();
//        TODO discuss about enrollments what is the correct way of enrolling
            for (Student s: this.studentService.findAll()) {
                for (int i = 0; i < 5; i ++){
                    StudentCourses studentCourses = this.studentCoursesRepository.save(new StudentCourses());
                    studentCourses.setStudent(s);
                    studentCourses.setCourseGroup(courseGroups.get(rand.nextInt(courseGroups.size())));
                    this.studentCoursesRepository.save(studentCourses);
                }

            }

    }
}
