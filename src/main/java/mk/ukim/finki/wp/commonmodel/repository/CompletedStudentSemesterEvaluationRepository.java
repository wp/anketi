package mk.ukim.finki.wp.commonmodel.repository;

import mk.ukim.finki.wp.commonmodel.base.Semester;
import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.evaluation.CompletedStudentSemesterEvaluation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompletedStudentSemesterEvaluationRepository extends JpaRepository<CompletedStudentSemesterEvaluation, Long> {

    Page<CompletedStudentSemesterEvaluation> findCompletedStudentSemesterEvaluationBySemesterAndStudent(Semester semester, Student student, Pageable pageable);

    Page<CompletedStudentSemesterEvaluation> findByOrderBySemesterDesc(Pageable pageable);

    Page<CompletedStudentSemesterEvaluation> findAllBySemester(Semester semester, Pageable pageable);

    Page<CompletedStudentSemesterEvaluation> findAllByStudent(Student student, Pageable pageable);

}