package mk.ukim.finki.wp.commonmodel.service.impl;

import mk.ukim.finki.wp.commonmodel.base.StudyProgram;
import mk.ukim.finki.wp.commonmodel.repository.StudyProgramRepository;
import mk.ukim.finki.wp.commonmodel.service.StudyProgramService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudyProgramServiceImpl implements StudyProgramService {

    private final StudyProgramRepository studyProgramRepository;

    public StudyProgramServiceImpl(StudyProgramRepository studyProgramRepository) {
        this.studyProgramRepository = studyProgramRepository;
    }

    @Override
    public List<StudyProgram> findAll() {
        return this.studyProgramRepository.findAll();
    }
}
