package mk.ukim.finki.wp.commonmodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class AnketiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnketiApplication.class, args);
    }

}
