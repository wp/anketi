package mk.ukim.finki.wp.commonmodel.repository;

import mk.ukim.finki.wp.commonmodel.base.CourseGroup;
import mk.ukim.finki.wp.commonmodel.base.Student;
import mk.ukim.finki.wp.commonmodel.base.StudentCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentCoursesRepository extends JpaRepository<StudentCourses, Long> {
    List<StudentCourses> findAllByStudent(Student student);

    List<StudentCourses> findAllByCourseGroup(CourseGroup courseGroup);

}
