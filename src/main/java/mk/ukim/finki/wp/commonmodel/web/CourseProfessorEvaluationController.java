package mk.ukim.finki.wp.commonmodel.web;

import mk.ukim.finki.wp.commonmodel.base.*;
import mk.ukim.finki.wp.commonmodel.evaluation.CompletedStudentSemesterEvaluation;
import mk.ukim.finki.wp.commonmodel.evaluation.CourseProfessorEvaluation;
import mk.ukim.finki.wp.commonmodel.service.*;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/survey")
public class CourseProfessorEvaluationController {

    private final CourseProfessorEvaluationService courseProfessorEvaluationService;
    private final StudentService studentService;
    private final ProfessorService professorService;
    private final CourseService courseService;

    private final StudentCoursesService studentCoursesService;
    private final CompletedStudentSemesterEvaluationService completedStudentSemesterEvaluationService;

    public CourseProfessorEvaluationController(CourseProfessorEvaluationService courseProfessorEvaluationService,
                                               StudentService studentService, ProfessorService professorService,
                                               StudentCoursesService studentCoursesService,
                                               CompletedStudentSemesterEvaluationService completedStudentSemesterEvaluationService,
                                               CourseService courseService) {
        this.courseProfessorEvaluationService = courseProfessorEvaluationService;
        this.completedStudentSemesterEvaluationService = completedStudentSemesterEvaluationService;
        this.studentService = studentService;
        this.professorService = professorService;
        this.courseService = courseService;
        this.studentCoursesService = studentCoursesService;
    }

    @GetMapping
    public String getAllSurveys(@RequestParam(required = false) String courseCode,
                                @RequestParam(required = false) String professorId,
                                @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                @RequestParam(required = false, defaultValue = "2") Integer pageNumberOfItems,
                                Model model) {

        Page<CourseProfessorEvaluation> courseProfessorEvaluations = null;

        if ((courseCode == null && professorId == null) ||
                (Objects.equals(courseCode, "null") && Objects.equals(professorId, "null"))) {
            courseProfessorEvaluations = this.courseProfessorEvaluationService.findAllByPage(pageNum, pageNumberOfItems);
        } else {
            courseProfessorEvaluations = this.courseProfessorEvaluationService.filter(courseCode, professorId, pageNum, pageNumberOfItems);
        }


        List<Course> courses = this.courseService.findAll();
        List<Professor> professors = this.professorService.findAllSorted();

        model.addAttribute("courseProfessorEvaluations", courseProfessorEvaluations.getContent());
        model.addAttribute("currentPage", courseProfessorEvaluations.getNumber() + 1);
        model.addAttribute("totalPages", courseProfessorEvaluations.getTotalPages());

//       nezavisni od strani gi zemame site zaradi filterot ako nema potreba ke gi izbriseme
        model.addAttribute("courses", courses);
        model.addAttribute("professors", professors);

        model.addAttribute("filterCourseCode", courseCode);
        model.addAttribute("filterProfessorId", professorId);

        return "surveysHomePage";
    }

    @GetMapping("/{semester}/{index}")
    public String getStudentSurveys(
            @PathVariable String semester,
            @PathVariable String index,
            Model model) {
        Page<CompletedStudentSemesterEvaluation> completedStudentSemesterEvaluation =
                this.completedStudentSemesterEvaluationService.
                        findCompletedSemesterEvaluationBySemesterAndStudent(semester, index, 1, 1);

        if (completedStudentSemesterEvaluation != null && completedStudentSemesterEvaluation.getTotalElements() > 0) {
            model.addAttribute("semester", semester);
            model.addAttribute("index", index);
            return "studentSurveyAlredyCompleted";
        }

        Student student = this.studentService.findByIndex(index);
        List<StudentCourses> studentCourses = this.studentCoursesService.findAllByStudent(student);
        List<Course> courses = new ArrayList<>();
        List<Professor> professors = new ArrayList<>();

        for (StudentCourses studentCourse : studentCourses) {
            Course course = studentCourse.getCourseGroup().getCourse();

            if (course.getSemester().getCode().equals(semester)) {
                courses.add(course);
                professors.add(studentCourse.getCourseGroup().getProfessor());
            }
        }

        model.addAttribute("courses", courses);
        model.addAttribute("professors", professors);
        model.addAttribute("student", student);
        model.addAttribute("semester", semester);

        return "surveyAnswearForm";
    }

    @PostMapping("/{semester}/{index}")
    public String submitSurvey(@PathVariable(required = false) String semester,
                               @PathVariable(required = false) String index,
                               @RequestParam(required = false) List<String> coursesIds,
                               @RequestParam(required = false) List<String> professorIds,
                               @RequestParam String grades0,
                               @RequestParam(required = false) String grades1,
                               @RequestParam(required = false) String grades2,
                               @RequestParam(required = false) String grades3,
                               @RequestParam(required = false) String grades4,
                               @RequestParam(required = false) String grades5,
                               @RequestParam(required = false) List<String> comments
    ) {


        List<String> grades = Arrays.asList(grades0, grades1, grades2, grades3, grades4, grades5);

        List<Course> courses = this.courseService.findAllByIds(coursesIds);
        List<Professor> professors = this.professorService.findAllByIds(professorIds);

        boolean isSaveComplete = true;

        for (int i = 0; i < courses.size(); i++) {
            try {
                this.courseProfessorEvaluationService.save(
                        courses.get(i),
                        professors.get(i),
                        Short.parseShort(grades.get(i)),
                        comments.get(i));
            } catch (Exception e) {
                isSaveComplete = false;
            }
        }
        if (isSaveComplete) {
            this.completedStudentSemesterEvaluationService.save(semester, index);
        }

        return "redirect:/survey";
    }
}
