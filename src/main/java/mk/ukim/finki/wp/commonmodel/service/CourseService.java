package mk.ukim.finki.wp.commonmodel.service;


import mk.ukim.finki.wp.commonmodel.base.Course;

import java.util.List;

public interface CourseService {

    Course save(Course course);

    List<Course> findAll();

    List<Course> findAllByIds(List<String> courseIds);

    Course findById(String courseId);

    List<Course> findAllCoursesContainingSubject(String subjectId);
}
